scriptencoding utf-8
" ^^ Please leave the above line at the start of the file.

" {{{ General settings
set backspace=indent,eol,start
set hidden
set history=1000
set nocompatible
set shellslash
set undolevels=1000
" }}}

" {{{ Editor settings
set autoindent
set colorcolumn=80
set expandtab
set foldmethod=marker
set guifont="Monospace 10"
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set nowrap
set number
set numberwidth=3
set ruler
set shiftround
set shiftwidth=4
set showcmd
set showmatch
set showmode
set smartcase
set softtabstop=4
set tabstop=4
" }}}

" {{{ Miscellaneous settings
filetype plugin indent on
set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=L
set modeline
set nobackup
set noswapfile
set shortmess+=I
set title
set viminfo='20,\"500
set wildignore=*.swp,*.bak,*.pyc,*.a,*.class,*.info,*.aux,*.log,*.dvi,
    \*.bbl,*.out,*.o,*.lo
set wildmode=list:longest,full
syntax on
" }}}

" {{{ Plugins
"   {{{ Pathogen
runtime bundle/pathogen/autoload/pathogen.vim
execute pathogen#infect()
"   }}}
"   {{{ NERDTree
noremap <Leader>f :silent NERDTreeToggle<CR>
let NERDTreeIgnore = ['\.o$', '\.in$', '^tags$']
"   }}}
"   {{{ SuperTab
let g:SuperTabMappingForward = "<C-p>"
let g:SuperTabMappingBackward = "<C-n>"
let g:snips_author="Emerson Max de Medeiros Silva"
"   }}}
"   {{{ fugitive
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gl :Glog<CR>
noremap <Leader>gp :Git push<CR>
"   }}}
"   {{{ ctrlp
let g:ctrlp_switch_buffer = ""
"   }}}
" }}}

" {{{ Theme
set background=dark
colorscheme solarized
" }}}

" {{{ Shortcuts
noremap <S-F1> :!
inoremap <S-F1> <C-O>:!
noremap <F2> :e<Space>
inoremap <F2> <C-O>:e<Space>
noremap <S-F2> :w<Space>
inoremap <S-F2> <C-O>:w<Space>
noremap <F3> :w<CR>
inoremap <F3> <C-O>:w<CR>
noremap <S-F3> :wa<CR>
inoremap <S-F3> <C-O>:wa<CR>
noremap <Leader>mks :mksession! .vimsession<CR>
noremap <Leader>/ :nohlsearch<CR>
cmap w!! w !sudo tee > /dev/null %
" }}}

" {{{ Habit break
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>
" }}}

" {{{ Auto commands
autocmd BufWritePre * :%s/\s\+$//e
autocmd BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl setf glsl
autocmd BufNewFile,BufRead .vimproject setf vim
" }}}

