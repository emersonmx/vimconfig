" Shortcuts
noremap <F5> :!python %<CR>
noremap <S-F5> :!python %<Space>
inoremap <F5> <C-O>:!python %<CR>
inoremap <S-F5> <C-O>:!python %<Space>
